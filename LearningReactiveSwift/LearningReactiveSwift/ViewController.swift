//
//  ViewController.swift
//  LearningReactiveSwift
//
//  Created by herbal7ea on 2018/10/31.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit
import Result
import ReactiveSwift


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Example.instance.run()
    }
}



class Example {
    static var instance = Example()
    
    func run() {
        let signal = Signal<Int, NoError> { (observer, lifetime) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                observer.send(value: 1)
                observer.send(value: 2)
                observer.send(value: 3)
                observer.sendCompleted()
            }
        }

        signal.observe { event in
            print("🦄 \(event)")
        }
        // VALUE 1
        // VALUE 2
        // VALUE 3
    }
}
